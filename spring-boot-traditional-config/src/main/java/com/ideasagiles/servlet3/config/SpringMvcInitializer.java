package com.ideasagiles.servlet3.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Clase para inicializar Spring sin necesidad del web.xml
 * Esta clase reemplaza efectivamente al archivo web.xml, y se encarga de
 * configurar el DispatcherServlet atendiendo en las url indicadas.
 */
public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{AppConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
