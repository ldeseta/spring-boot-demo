package com.ideasagiles.servlet3.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Esta clase es usada por Spring Security para inicializarse.
 * Spring Security busca esta implementación para registrar los filters y
 * configurarse.
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
